# Setup
 
Clone parent repo first, then clone this as a submodule in the `cordova` folder.

in `cordova/`, (and after setting up the android SDK, etc):

```bash
mkdir www/
cordova platform add android
    
./debug.sh
```
To run a debug version on a phone connected by USB. 
    
## Google Play deploy

*Update version in `config.xml`*, if this doesn't get included, then the following steps will be wasted.


```
cordova build android --release

# change to the dir where the .apk was generated

# make sure `sensit.keystore` is in the same folder, this is needed for release, it's not committed into repo.

# then sign
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore sensit.keystore app-release-unsigned.apk sensit

zipalign -v 4 app-release-unsigned.apk app-release-signed-aligned.apk

```

Then upload in Google Play Console.